package ru.niit.client;

import ru.niit.User;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class WelcomeWindow extends JFrame {
    private JButton settingsButton;
    private JButton logInButton;
    private JList contactList;
    private JLabel welcomeMessage;
    private JPanel welcomeWindow;
    private static Lock lock = new ReentrantLock();
    private static Lock updateLock = new ReentrantLock();
    private static Condition updateCondition = updateLock.newCondition();
    private ClientHelper client = ClientHelper.getInstance();


    public WelcomeWindow() {
        this.setContentPane(welcomeWindow);

        logInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onLogin();
            }
        });
        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SettingsDialog(WelcomeWindow.this);
            }
        });
        contactList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                logInButton.setEnabled(true);
            }
        });
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                ClientHelper client = ClientHelper.getInstance();
                client.saveUserProperties();
                client.saveClientProperties();
                if (client.getCurrentUser() != null)
                    client.saveHistory();
            }
        });


        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        setLocationRelativeTo(null);
        this.setVisible(true);
        connect();
    }

    public void connect() {

        try (Socket socket = new Socket(client.getClientProperties().getServerIP(),
                client.getClientProperties().getServerPort())) {
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            System.out.println("send hello to server" + client.getClientProperties().getServerIP() + ":" +
                    client.getClientProperties().getServerPort());
            writer.println("hello");
            writer.flush();
            String userListMessage = reader.readLine();
            client.addUsers(userListMessage.replace("usersUpdateList/", ""));
            updateUserList(client.getUsers().values());
            welcomeMessage.setText("Выберите пользователя");


        } catch (ConnectException e) {
            System.err.println("connection failed");
            welcomeMessage.setText("Не удалось соединиться с сервером");
            if (contactList.getModel() instanceof DefaultListModel)
                ((DefaultListModel) contactList.getModel()).removeAllElements();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void onLogin() {
        //User currentUser = ClientHelper.getInstance().getCurrentUser();
        //if(currentUser==null || !nameField.getText().equals(currentUser.getName()))
        try {
            if (ClientHelper.getInstance().tryLogin(((User) contactList.getSelectedValue()).getName().replace("-offline", ""), "password")) {

                client.setMainWindow(ClientMainWindow.getInstance());
                ClientMainWindow.getInstance().setVisible(true);
                new ServerHandler(ClientHelper.clientSocket);
            } else {
                new SystemInfoDialog("Подключение под этим именем невозможно");
                return;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        dispose();

    }

    /* обновление списка контактов */
    public void updateUserList(Collection<User> userList) {
        updateLock.lock();
        try {
            ClientHelper client = ClientHelper.getInstance();
            List<User> list = new ArrayList<>(userList);
            Collections.sort(list);
            DefaultListModel<User> model = new DefaultListModel<>();

            for (User user : list) {
                if (!user.equals(client.getCurrentUser()) && user.getId() != 0)
                    model.addElement(user);
            }

            contactList.setModel(model);
        } finally {
            updateCondition.signal();
            updateLock.unlock();
        }
    }
}
