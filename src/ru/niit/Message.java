package ru.niit;

import java.io.Serializable;

public interface Message extends Serializable {

    public void setMessage(int fromID, int toID, MessageContent mess);

    public int getFromID();

    public void setFromID(int From);

    public int getToID();

    public void setToID(int To);

    public String getString();

    public MessageContent getContent();

    public String getDate();

}
