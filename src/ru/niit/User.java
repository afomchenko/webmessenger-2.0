package ru.niit;

public interface User extends Comparable<User> {
    public String getName();

    public boolean authorize(String password) throws UnsupportedOperationException;

    public boolean isOnline();

    public int getId();

    public String compress();

    void getOnline(boolean online);

    void changePassword(String password);
}
