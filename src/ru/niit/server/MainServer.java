package ru.niit.server;

import java.io.IOException;

public class MainServer {
    /* входная точка сервера */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        MessServer.getInstance();
        LoginService.getInstance(8888);
        ServerAdmin.doAdmin();
    }
}
