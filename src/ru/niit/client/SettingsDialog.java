package ru.niit.client;

import javax.swing.*;
import java.awt.event.*;

/* диалог установки настроек соединения*/
public class SettingsDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonCancel;
    private JTextField ipField;
    private JTextField tcpField;
    private JButton buttonApply;
    private JPanel serverSettings;
    WelcomeWindow welcomeWindow;


    public SettingsDialog(WelcomeWindow welcomeWindow) {

        this.welcomeWindow = welcomeWindow;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonApply);

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        buttonApply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                apply();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        ipField.setText(ClientHelper.getInstance().getClientProperties().getServerIP());
        tcpField.setText("" + ClientHelper.getInstance().getClientProperties().getServerPort());

        this.pack();
        setLocationRelativeTo(null);
        this.setVisible(true);

    }


    private void apply() {
        ClientHelper client = ClientHelper.getInstance();
        System.out.println("settings changed");

        client.getClientProperties().setServerIP(ipField.getText());
        try {
            client.getClientProperties().setServerPort(Integer.parseInt(tcpField.getText()));
        } catch (NumberFormatException ignored) {
        }
        welcomeWindow.connect();
        dispose();
    }


    private void onCancel() {
        dispose();
    }

}
