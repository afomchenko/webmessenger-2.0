package ru.niit;

/* конкретная реализация соджержимого сообщения со строкой*/
public class UserMessageContent implements MessageContent<String> {
    private String content;

    public UserMessageContent(String content) {
        this.content = content;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public void setContent(String content) {
    }

    @Override
    public String toString() {
        return content;
    }
}
