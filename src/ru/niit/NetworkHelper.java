package ru.niit;


import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/* Набор статических методов для пересылки и получения сообщений */
public class NetworkHelper {

    private NetworkHelper() {
    }

    /* получение сообщения из сокета
     * сообщение передается одной строкой,
      * разделенной спецсимволами*/
    public static Message recieveFromSocket(Socket socket) throws IOException {
        BufferedReader reciever = new BufferedReader(new InputStreamReader(socket.getInputStream(),  StandardCharsets.UTF_8));
        String inputStr = reciever.readLine();
        if (inputStr == null) return null;
        //System.out.println("recieve" + socket +inputStr);
        String[] messStr = inputStr.split("\u001f");
        if (messStr.length >= 3)
            return new UserMessage(Integer.parseInt(messStr[0].replaceAll("[\\D]", "")),
                    Integer.parseInt(messStr[1]),
                    new UserMessageContent(messStr[2]),
                    messStr[3]);
        else return null;
    }

    /* отправка сообщения в сокет */
    public static void sendToSocket(Socket socket, Message message) throws IOException {

        if (socket != null && !socket.isClosed()) {
            PrintWriter sender = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(),  StandardCharsets.UTF_8));

            sender.println(message.getString());
            //System.out.println("send" + socket + message);
            sender.flush();
        }
    }

    public static void sendString(Socket socket, String string) throws IOException {

        if (socket != null && !socket.isClosed()) {
            PrintWriter sender = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(),  StandardCharsets.UTF_8));

            sender.println(string);
            //System.out.println("send" + socket + message);
            sender.flush();
        }
    }
}
