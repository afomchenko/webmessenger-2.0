package ru.niit.server;


import ru.niit.User;
import ru.niit.UserMessageContent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ServerAdmin{
    private static MessServer server = MessServer.getInstance();

    private ServerAdmin() {}

    public static String adminMenu(){
        System.out.println("Server menu.");
        System.out.println("1 - create new user");
        System.out.println("2 - delete user");
        //System.out.println("3 - send message to users");
        System.out.println("3 - list of users");
        System.out.println("0 - close server");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("select menu: ");
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "0";
    }

    public static void doAdmin(){
        while(true)
        switch (adminMenu()){
            case "1": createUserConsole(); break;
            case "2": deleteUserConsole(); break;
            case "3": listUsersConsole(); break;
            case "0": System.exit(0); break;
        }
    }

    public static void createUserConsole(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            //System.out.println("Enter password [password]:");
            //String password = reader.readLine();
            if(server.getUserByName(name)==null) {
                server.addUser(name, "password");
                System.out.println("User " + name +" created.");
            }
            else System.out.println("User " + name +" is already exists.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteUserConsole(){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();

            User user = server.getUserByName(name);
            if(user!=null) {
                server.deleteUser(user);
                System.out.println("User " + name +" deleted.");
            }
            else System.out.println("User " + name +" is not exists.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void listUsersConsole(){
        for (User user : server.getUsers().values()) {
            System.out.println(user.getId()+" - "+user);
        }

    }


}


