package ru.niit.client;

import javax.sound.midi.*;

/**
 * Created by Roman on 13.10.2014.
 */
public class MIDIplayer {
    private MIDIplayer() {
    }

    public static void play(final int instrument, final int note) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Sequencer player = MidiSystem.getSequencer();
                    player.open();

                    Sequence seq = new Sequence(Sequence.PPQ, 4);

                    Track track = seq.createTrack();

                    try {
                        ShortMessage instr = new ShortMessage();
                        instr.setMessage(192, 1, instrument, 0);
                        MidiEvent changeInstr = new MidiEvent(instr, 1);
                        track.add(changeInstr);
                    } catch (Exception exInstrument) {
                        System.out.println("This instrument was not found! Maximum size is 127. Default tool is piano");
                    }
                    try {
                        // for (int i = 50; i > 0; i-=10) {

                        ShortMessage a = new ShortMessage();
                        a.setMessage(144, 1, note, 100);
                        MidiEvent noteOn = new MidiEvent(a, 1);
                        track.add(noteOn);

                        ShortMessage b = new ShortMessage();
                        b.setMessage(128, 1, note, 100);
                        MidiEvent noteOff = new MidiEvent(b, 5);
                        track.add(noteOff);
                        // }

                    } catch (Exception exNote) {
                        System.out.println("This note was not found! Maximum size is 127");
                    }

                    player.setSequence(seq);
                    player.setTempoInBPM(100);
                    player.start();

                    while (player.isRunning()) {
                        Thread.sleep(1500);
                    }
                    player.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();


    }
}
