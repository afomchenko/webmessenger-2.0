package ru.niit;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserImpl implements User, Comparable<User> {


    private final int id;
    private String name;
    private String password;
    private boolean online;


    public UserImpl(int id, String name, String password, boolean isPassCompressed) {
        this.id = id;
        this.name = name;

        if(isPassCompressed)
            this.password = password;
        else try {
            this.password=encryptPassword(password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        online = false;
    }

    public UserImpl(int id, String name, boolean online) {
        this.id = id;
        this.name = name;
        this.online = online;
    }

    @Override
    public String getName() {
        return name;
    }

    /* сравнение пароля с запрошеным */
    @Override
    public boolean authorize(String password) {
        try {
            if (this.password.equals(encryptPassword(password))) return true;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* проверка онлайн ли пользователь */
    @Override
    public boolean isOnline() {
        if (!online)
            return false;
        else
            return true;
    }

    /* получить идентификатор */
    @Override
    public int getId() {
        return id;
    }

    @Override
    public String compress() {
        return this.id+":"+this.name+":"+this.password;
    }

    public static User uncompress(String compressed){
        String [] userStr = compressed.split(":");
        return new UserImpl(Integer.parseInt(userStr[0]), userStr[1], userStr[2], true);
    }

    public static String encryptPassword(String password)
            throws NoSuchAlgorithmException {
        MessageDigest messageDigest =
                MessageDigest.getInstance("MD5");
        byte[] bs;

        messageDigest.reset();
        bs = messageDigest.digest(password.getBytes());

        StringBuilder stringBuilder = new StringBuilder();

        //шестнадцатеричное кодирование дайджеста
        for (int i = 0; i < bs.length; i++) {
            String hexVal = Integer.toHexString(0xFF & bs[i]);
            if (hexVal.length() == 1) {
                stringBuilder.append("0");
            }
            stringBuilder.append(hexVal);
        }

        return stringBuilder.toString();
    }

    /* Установка пользователя онлайн */
    @Override
    public void getOnline(boolean online) {
        this.online = online;
    }

    @Override
    public void changePassword(String password) {
        try {
            this.password = encryptPassword(password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserImpl)) return false;

        UserImpl user = (UserImpl) o;

        if (id != user.id) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;

        return true;
    }



    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(User o) {
        if (!this.isOnline() && o.isOnline()) return 1;
        else if (this.isOnline() && !o.isOnline()) return -1;
        else return this.name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        if (!isOnline())
            return name + " " + " - offline";
        else return name;
    }


}
