package ru.niit.server;

import ru.niit.*;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Map;

/* класс содержит набор дополнительных статических методов,
 * необходимых для работы сервера */
public class ServerHelper {

    private static Object lock = new Object();

    /* перенаправление полученного сообщения */
    public static void sendMessage(Message mess) {
        MessServer server = MessServer.getInstance();
        try {
            if (mess != null && mess.getToID() != 0) {
                User recipient = MessServer.getInstance().getUsers().get(mess.getToID());
                if (recipient != null && recipient.isOnline()) {
                    NetworkHelper.sendToSocket(server.getSocket(recipient), mess);
                } else if (recipient == null) {
                    sendSystemMessage(MessServer.getInstance().getUsers().get(mess.getFromID()),
                            new UserMessageContent("cannot send, unknown user " + mess.getToID()));
                } else {
                    sendSystemMessage(MessServer.getInstance().getUsers().get(mess.getFromID()),
                            new UserMessageContent("cannot send, user " + mess.getToID() + " offline "));
                    server.addUnread(mess.getToID(), mess);
                }

            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    /* отправка служебного сообщения клиенту */
    public static void sendSystemMessage(User recipient, MessageContent content) {
        MessServer server = MessServer.getInstance();
        if (server.getSocket(recipient) == null) return;
        try {
            if (recipient.isOnline()) {
                NetworkHelper.sendToSocket(server.getSocket(recipient), new UserMessage(0, recipient.getId(), content));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    /* отправка служебного сообщения всем */
//    public static void sendSystemEverywhere(MessageContent content) {
//        for (User user : MessServer.getInstance().getUsersOnline().values()) {
//            sendSystemMessage(user, content);
//        }
//    }

    /* отправка служебного сообщения, что пользователь вошел или вышел */
    public static void sendUserOnlineEverywhere(User u) {

        MessServer server = MessServer.getInstance();
        for (User recipient : MessServer.getInstance().getUsersOnline().values()) {

            if (server.getSocket(recipient) == null) continue;
            try {
                if (recipient.isOnline()) {
                    if (u.isOnline()) {
                        NetworkHelper.sendToSocket(server.getSocket(recipient), new UserMessage(0, u.getId(), new UserMessageContent(u.getName() + " is Online")));
                    } else {
                        NetworkHelper.sendToSocket(server.getSocket(recipient), new UserMessage(0, u.getId(), new UserMessageContent(u.getName() + " is Offline")));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /* отправка служебного сообщения со списком пользователей всем  */
    public static void updateUserList() {

        String userList = buildUserList();

        for (User user : MessServer.getInstance().getUsersOnline().values()) {
            sendSystemMessage(user, new UserMessageContent(userList));
        }
    }

    /* отправка служебного сообщения со списком пользователей */
    public static void sendUserListString(Socket socket) {

        String userList = buildUserList();

        try {
            NetworkHelper.sendString(socket, userList);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private static String  buildUserList(){
        StringBuilder userList = new StringBuilder("usersUpdateList");
        Map<Integer, User> users = MessServer.getInstance().getUsers();
        Map<Integer, User> usersOnline = MessServer.getInstance().getUsersOnline();
        for (Map.Entry<Integer, User> entry : users.entrySet()) {
            if (usersOnline.containsKey(entry.getKey())) {
                userList.append('/').append(entry.getValue().getName()).append(':').append(entry.getValue().getId());
            } else {
                userList.append('/').append(entry.getValue().getName()).append("-offline").append(':').append(entry.getValue().getId());
            }
        }
        return userList.toString();
    }
}
