package ru.niit.server;

import ru.niit.Message;
import ru.niit.NetworkHelper;
import ru.niit.User;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

/* Класс-слушатель для входящих сообщений от клиентов
 * запускается в отдельном потоке */
public class ClientHandler implements Runnable {

    protected Thread t;
    MessServer server;
    private User user;
    private Socket currentSocket;

    public ClientHandler(User user) {
        server = MessServer.getInstance();
        this.user = user;
        currentSocket = server.getSocket(user);
        t = new Thread(this);
        t.setDaemon(true);
        t.start();
    }

    @Override
    public void run() {
        try {
            server.setUserOnline(user);
            ServerHelper.updateUserList();
            Thread.sleep(300);
            ServerHelper.sendUserOnlineEverywhere(user);
            Thread.sleep(300);
            server.sendUnread(user);

            while (currentSocket != null && !currentSocket.isClosed()) {
                Message mess = null;
                try {
                    mess = NetworkHelper.recieveFromSocket(server.getSocket(user));

                } catch (SocketException e) {
                    //System.err.println("Socket closed");
                    if (server.getSocket(user) != null)
                        server.getSocket(user).close();
                    server.setUserOffline(user);

                    ServerHelper.sendUserOnlineEverywhere(user);
                    return;
                }
                ServerHelper.sendMessage(mess);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                if (currentSocket != null && !currentSocket.isClosed())
                    currentSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (user!=null && server.getSocket(user)==null) server.setUserOffline(user);
            ServerHelper.updateUserList();
        }
    }
}
